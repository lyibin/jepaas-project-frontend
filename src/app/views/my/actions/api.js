// 服务器地址
import { HOST_RBAC, HOST_BASIC } from '../../../constants/config';

// 上传头像
export const POST_DO_UPDATE = `${HOST_BASIC}/je/document/fileBase64`;
export const POST_DO_SAVE = `${HOST_BASIC}/je/rbac/user/doUpdateInfo`;
// 获取用户信息
// export const POST_USER_INFO = `${HOST_BASIC}/je/pc/register/registerAccount`;
// 取消绑定
export const POST_CANCEL_BIND = `${HOST_BASIC}/je/rbac/user/doCallAt`;
// app    修改路径，将39服务器改成dev.suanbanyun.cHOST_BASICom
export const POST_APP_CREATE_STATE = `${HOST_BASIC}/je/saas/saasYh/createState`;

// 钉钉获取token
export const GET_DINGTALK_TOKEN = 'https://oapi.dingtalk.com/sns/gettoken';

export const GET_DINGTALK_PERSISTENT_CODE = 'https://oapi.dingtalk.com/sns/get_persistent_code';
// 获取钉钉用户信息
export const GET_USER_MESS = `${HOST_BASIC}/je/saas/saasYh/getUserAt`;
// 三方认证
export const POST_APP_OTHER_LOGIN = `${HOST_BASIC}/je/saas/saasYh/authDsfInfo`;
// 从钉钉获取openid
export const POST_APP_DTOPENID = `${HOST_BASIC}/je/saas/saasYh/getAuthDT`;
export const POST_GET_USER_INFO_BY_ID = `${HOST_BASIC}/je/rbac/user/getCurrentUserInfo`;
