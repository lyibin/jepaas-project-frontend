import fetch from '@/helper/httpUtil';
import {
  POST_CANCEL_BIND,
  POST_APP_CREATE_STATE,
  GET_DINGTALK_TOKEN,
  GET_DINGTALK_PERSISTENT_CODE,
  POST_APP_OTHER_LOGIN,
  GET_USER_MESS,
  POST_APP_DTOPENID,
  POST_DO_UPDATE,
  POST_DO_SAVE,
  POST_GET_USER_INFO_BY_ID,
} from './api';

// 获取用户最新信息
export function sendBubble(param) {
  return fetch(POST_GET_USER_INFO_BY_ID, null, {
    type: 'POST',
    data: param,
  })
    .then(data => data)
    .catch();
}

/**
 *
 *  压缩图片
 *
 * @export
 * @param {path} string 图片位置
 * @returns {Promise }
 */
export function zipImg(path) {
  return new Promise((resolve, reject) => {
    plus.zip.compressImage({
      src: path,
      dst: `_doc/temp/${+new Date()}.jpg`,
      quality: 70,
    },
    ({ target }) => {
      resolve(target);
    },
    (e) => {
      reject(e);
    });
  });
}
/**
 *
 *  上传图片
 *
 * @export
 * @param {*} { path, hideWaiting }
 *  1、压缩图片，{
      PHOTO: item,
      tableCode: 'JE_CORE_ENDUSER',
      USERID: JE.getCurrentUser().id,
      DEPTID: JE.getCurrentUser().deptId,
      funcCode: 'JE_CORE_ENDUSER',
      uploadableFields: 'PHOTO',
      jeFileType: 'PROJECT',
    }
 *
 * @returns {Promise ALL}
 */
export function uploaderImg(params) {
  return fetch(POST_DO_UPDATE, null, {
    type: 'POST',
    data: params,
  })
    .then(data => data)
    .catch();
}
export function saveUserImg(params) {
  return fetch(POST_DO_SAVE, null, {
    type: 'POST',
    data: params,
  })
    .then(data => data)
    .catch();
}
/**
 *取消绑定
 * @param param
 * @returns {Promise<T | never>}
 */
export function cancelBind(param) {
  return fetch(POST_CANCEL_BIND, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}
/**
 * 发送状态机
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchCreateState(param) {
  return fetch(POST_APP_CREATE_STATE, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}
/**
 *获取钉钉token
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetcDingTalkToken(param) {
  return fetch(GET_DINGTALK_TOKEN, param, {})
    .then(data => data)
    .catch();
}
/**
 * 获取钉钉持久授权码  url?access_token
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchDingTalkPersistent(getParam, param) {
  return fetch(GET_DINGTALK_PERSISTENT_CODE, getParam, {
    type: 'post',
    contentType: 'application/json; charset=UTF-8',
    data: param,
  })
    .then(data => data)
    .catch();
}
/**
 *三方绑定
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchOtherLogin(param) {
  return fetch(POST_APP_OTHER_LOGIN, param, {
    type: 'post',
    // contentType: 'application/json; charset=UTF-8',
    // data: param
  })
    .then(data => data)
    .catch();
}
/**
 *获取用户信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchUserMess(param) {
  return fetch(GET_USER_MESS, '', {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}
/**
 *获取钉钉openid
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchOpenid(param) {
  return fetch(POST_APP_DTOPENID, '', {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}
