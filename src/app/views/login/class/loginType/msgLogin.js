/*
 * @Descripttion:短信验证码登录
 * @Author: 张明尧
 * @Date: 2021-01-25 15:50:07
 * @LastEditTime: 2021-02-02 16:41:40
 */
import BasicsLogin from './basics';
import { fetchCheckValidate } from '../../actions/login';
// 继承基础的登录类
export default class MsgLogin extends BasicsLogin {
  constructor({vm}) {
    super(vm);
    this.param = {};
  }

  /**
   * 设置param的参数值(重写掉基础类的方法)
   * @param {Object} loginInfo
   */
  setParam(loginInfo) {
    this.param = {
      j_username: loginInfo.phone,
      j_code: loginInfo.validateCode,
      state: '',
      loginType: 'PHONE',
      isNew: 1,
    }
  }

   /**
   * 校验当前验证码
   */
  verifyPhoneMsg() {
    return fetchCheckValidate({
      email: this.param.j_username,
      type: 'PHONE',
      cz: 'LOGIN',
      code: this.param.j_code,
    }).then(res => res);
  }

  /**
   * 登录点击之前执行的方法（拓展方法）
   */
  beforeClick() {
    // 校验当前验证码是否通过
    return this.verifyPhoneMsg().then(msg => msg);
  }
}
