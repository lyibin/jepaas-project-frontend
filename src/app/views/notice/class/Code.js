import {
  getCode,
  getNoticeNumber,
  getNoticeRead,
  readAll,
  loadDailydetail,
  getInfoById,
} from '../actions/code';

export default class Code {
  /*
   * 获取钉钉的码
   */
  static async getBarCode(code) {
    const res = await getCode({
      code,
    });
    if (res.success) {
      return res.obj || [];
    }
    JE.msg(res.message);
    return [];
  }

  // 获取通知的未读数量以及列表
  static async getNoticeNumbers(params) {
    const res = await getNoticeNumber(
      params
    );
    return res || {};
  }

  // 获取通知的详情是否存在
  static async getInfoById(params) {
    const res = await getInfoById(
      params
    );
    return res || {};
  }

  // 获取通知的未读变为已读
  static async getNoticeReads(params) {
    const res = await getNoticeRead(
      params
    );
    if (res.success) {
      return res || {};
    }
    JE.msg(res.msg);
  }

  // 获取通知的未读变为已读
  static async readAll() {
    const res = await readAll();
    if (res.success) {
      return res || {};
    }
  }

  // load日志详情
  static async loadDailydetail(params) {
    const res = await loadDailydetail(params);
    if (res) {
      return res || {};
    }
  }
}
