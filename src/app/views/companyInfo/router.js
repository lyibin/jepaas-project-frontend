//  When I wrote this code, only God and I knew what it was. Now, only God knows!
/*
 * @Description:
 * @Author: yjp
 * @Date: 2020-10-10
 * @LastEditTime: 2020-10-10
 * @FilePath: /src/views/companyInfo/router.js
 */

import companyInfo from './index.vue';


export default [
  {
    path: '/JE-PLUGIN-COMPANYINFO',
    name: 'index',
    component: companyInfo,
  }];
