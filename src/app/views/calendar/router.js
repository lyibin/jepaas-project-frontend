/**
 * @Author : ZiQin Zhai
 * @Date : 2020/10/9 13:40
 * @Version : 1.0
 * @Last Modifined by : ZiQin Zhai
 * @Last Modifined time : 2020/10/9 13:40
 * @Description
 * */
import Index from './index.vue';
import calendardetail from './pages/calendardetail/index.vue';
import allschedule from './pages/allschedule/index.vue';
import rootPage from './rootPage';

export default [
  {
    path: '/JE-PLUGIN-CALENDAR',
    name: 'JE-PLUGIN-CALENDAR',
    component: rootPage,
    children: [
      {
        path: '/',
        component: Index,
      },
      {
        path:"calendardetail",
        component: calendardetail,
      },
      {
        path:"allschedule",
        component:allschedule
      }
    ],
  }
];
