// /*
//  * @Description: 
//  * @Author: lzp
//  * @Date: 2021-03-22 14:18:00
//  * @LastEditTime: 2021-03-22 14:18:20
//  * @LastEditors: xxx
//  */
// import Vue from 'vue';
// import VueLazyload from 'vue-lazyload';
// import App from './index.vue';

// Vue.use(VueLazyload);
// Vue.config.productionTip = false;
// Vue.config.devtools = true;
// new Vue({
//   render: h => h(App),
// }).$mount('#app');
import router from './router';
import config from './config.json';
import install from '../../util/install';

install(router, config);
