/*
 * @Descripttion:
 * @version:
 * @Author: qinyonglian
 * @Date: 2019-11-04 10:46:04
 * @LastEditors: qinyonglian
 * @LastEditTime: 2021-03-02 13:43:12
 */


// todo 还需要做一个操作就是tag标签的选中
import { fileSuffixIcon, getfilesize } from '../common/util';
import { checkTag } from '../common/constant';
/*
 * 基本模型类
 */
export default class BaseModel {
  constructor(options) {
    this.init(options);
  }

  /*
   * 映射属性
   */
  init(options) {
    // 以下是解构的两种方式，只不过options.
    // Object.assign(this, options);
    // 创建时间
    this.createTime = options.modifiedTime || options.createTime;
    // 文件的key
    this.fileKey = options.fileKey || null;
    // 唯一标示ID
    this.nodeId = options.nodeId || null;
    // 属于公司还是个人文件
    this.diskType = options.diskType || null;
    // 文件名称
    this.name = options.nodeName || null;
    // 修改时间
    this.uploadTime = options.modifiedTime || null;
    // 文件尾缀
    this.type = options.fileSuffix || null;
    // 文件大小
    this.size = getfilesize(options.fileSize) || null;
    // 标签
    this.nodeTag = options.nodeTag || null;
    // 父级id
    this.parent = options.parent || null;
    // 父级路径
    this.parentPath = options.parentPath || null;
    // 目录层次
    this.parentPathName = options.parentPathName || null;
    // 默认没有被选中
    this.checked = false;
    // 移动到item上的颜色
    this.backgroundCheck = false;
    this.checkTag = this.checkTag();

    // this._checkTag = JSON.parse(JSON.stringify(checkTag));
  }

  get icon() {
    const iconData = fileSuffixIcon(this.type);
    console.log(iconData);
    // if (iconData.isImg) {
    //   this.fileKey && (iconData.url = `${JE.buildDocUrl()}?fileKey=${this.fileKey}`);
    //   this.fileKey || (iconData.url = null);
    // }
    return iconData;
  }

  // set icon(val) {
  //   this.type = val;
  // }

  checkTag() {
    const checkTags = JSON.parse(JSON.stringify(checkTag));
    // TODO如果this.fileTag 中有值，那么要将checkTag中的值改变一下
    if (!this.nodeTag) return checkTags;
    const indexArr_ = this.nodeTag.split(',');
    indexArr_.forEach((item) => {
      checkTags[item - 1].check = true;
    });
    return checkTags;
  }


  /*
   * 创建对象
   */
  static create(option) {
    return new BaseModel(option);
  }
}
