/* eslint-disable new-cap */
/*
 * @Descripttion:
 * @version:
 * @Author: qinyonglian
 * @Date: 2019-11-04 14:52:24
 * @LastEditors: qinyonglian
 * @LastEditTime: 2019-12-06 11:54:54
 */
import BaseModel from './baseModel';

export default class recordModel extends BaseModel {
  constructor(option) {
    super(option); // 相当于获得父类的this指向,继承属性

    this._init(option);
  }


  _init(option) {
    // 如果是自己的私有属性
    this.uploadStatus = option.transferStatus; // 文件状态 success还是fail
    this.id = option.id; // 唯一标示ID
    this.operation = {
      text: '',
      icon: 'jeicon-trash-o',
    };
    // 选中的文件 或者文件夹有哪些操作功能
    this.docFunc = [
      { icon: 'jeicon-trash-o', clickText: 'remove' },
    ];
    this.transferType = option.transferType; // 上传还是下载
    this.isDelete = option.shareStatue;
  }

  get statusText() {
    let status = '';
    let type = '';
    if (this.uploadStatus == 'success') {
      status = '成功';
    } else {
      status = '失败';
    }
    if (this.transferType == 'upload') {
      type = '上传';
    } else {
      type = '下载';
    }
    return type + status;
  }

  /*
   * 创建对象
   */
  static create(options) {
    return new recordModel(options);
  }
}
